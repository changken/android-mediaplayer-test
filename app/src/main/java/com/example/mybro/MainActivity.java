package com.example.mybro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        Button button2 = (Button) findViewById(R.id.button2);
        Button ytBtn = (Button) findViewById(R.id.yt_btn);
        final TextView textView = (TextView) findViewById(R.id.tv1);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("林北但你啊!");

                //去撥放器
                startActivity(new Intent(MainActivity.this, PlayActivity.class));
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("我跟你談大海 你跟我談浴缸!");

                //去遠程撥放器
                startActivity(new Intent(MainActivity.this, RemotePlayActivity.class));
            }
        });

        ytBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TestYoutubePlayerActivity.class));
            }
        });
    }
}
