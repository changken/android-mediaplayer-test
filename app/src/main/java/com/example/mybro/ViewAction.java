package com.example.mybro;

import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class ViewAction {
    /* button here */
    // button disable
    public static void disableButton(Button btn){
        btn.setClickable(false);
        btn.setEnabled(false);
    }

    // button enable
    public static void enableButton(Button btn){
        btn.setClickable(true);
        btn.setEnabled(true);
    }

    public static void changeButtonText(Button btn, String text){
        btn.setText(text);
    }

    /* seekbar */
    public static void disableSeekBar(SeekBar seekBar){
        seekBar.setClickable(false);
        seekBar.setEnabled(false);
    }

    public static void enableSeekBar(SeekBar seekBar){
        seekBar.setClickable(true);
        seekBar.setEnabled(true);
    }

    public static void setSeekBarProgress(SeekBar seekBar, int progress){
        seekBar.setProgress(progress);
    }

    public static void setSeekBarMax(SeekBar seekBar, int max){
        seekBar.setMax(max);
    }

    /* textview */
    public static void changeTvText(TextView textView, String text){
        textView.setText(text);
    }
}
