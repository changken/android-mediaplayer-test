package com.example.mybro;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;

public class MediaPlayerManager {
    private MediaPlayer mMediaPlayer;
    private OnPreparedListener mOnPreparedListener;
    private OnErrorListener mOnErrorListener;
    private OnPlayOrPauseListener mOnPlayOrPauseListener;
    private Handler mHandler;

    //constructor
    public MediaPlayerManager(OnPreparedListener onPreparedListener, OnErrorListener onErrorListener, OnPlayOrPauseListener onPlayOrPauseListener) {
        // initialize some listener
        mOnPreparedListener = onPreparedListener;
        mOnErrorListener = onErrorListener;
        mOnPlayOrPauseListener = onPlayOrPauseListener;

        //delay handler which is provided by android
        mHandler = new Handler();
    }

    // check if media player existed
    public boolean checkIfMediaPlayerExisted() {
        return mMediaPlayer != null;
    }

    // delegate
    public boolean isPlaying() {
        return mMediaPlayer.isPlaying();
    }

    // delegate
    public void seekTo(int msec) {
        mMediaPlayer.seekTo(msec);
    }

    // delegate
    public int getDuration() {
        return mMediaPlayer.getDuration();
    }

    // delegate
    public int getCurrentPosition() {
        return mMediaPlayer.getCurrentPosition();
    }

    /* clear Media player instance */
    public void clearMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    /* create a Media player instance*/
    public void createMediaPlayer(String songUrl) {
        // if not enter a url
        if (ifNotEnterAUrl(songUrl)) {
            Log.e("MediaPlayerManager", "You have to provide an url!");
            return;
        }

        // if not enter a https url
        if (ifNotEnterAHttpsUrl(songUrl)) {
            Log.e("MediaPlayerManager", "You have to provide a \"https\" url!");
            return;
        }

        // clear the media player before you create another one.
        clearMediaPlayer();

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        // prepare media source by async way
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // execute the custom OnPreparedListener outside
                mOnPreparedListener.onPrepared(mp);

                Log.e("MediaPlayerManager", "準備好了!");
            }
        });
        // when the media player face some error, then it will come to here.
        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                // execute the custom OnErrorListener outside
                mOnErrorListener.onError(mp, what, extra);

                // clear the media player
                clearMediaPlayer();
                Log.e("MediaPlayerManager", mp.toString() + " what=" + what + " extra=" + extra);

                return true;
            }
        });

        try {
            // set data source by using url
            mMediaPlayer.setDataSource(songUrl);
            // prepare a song by using async way.
            mMediaPlayer.prepareAsync();
        } catch (IllegalStateException e) {
            Log.e("MediaPlayerManager", e.getMessage());
        } catch (IOException e) {
            Log.e("MediaPlayerManager", e.getMessage());
        }
    }

    /* pause or play a song*/
    public void playAndPauseTheSong() {
        try {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
                // execute the custom OnPlayOrPauseListener outside
                mOnPlayOrPauseListener.onPause();
            } else {
                mMediaPlayer.start();
                // execute the custom OnPlayOrPauseListener outside
                mOnPlayOrPauseListener.onPlay();
            }
        } catch (IllegalStateException e) {
            Log.e("MediaPlayerManager", e.getMessage());
        }
    }

    /* stop and replay the song */
    public void stopAndResetTheSong() {
        try {
            // if you want to cut song
            if (mMediaPlayer.isPlaying()) {
                //first, pause the song which is playing
                mMediaPlayer.pause();

                //second, "wait a second" to cut a song.
                //This step is very important!
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopAndPrepareAsync();
                    }
                }, 1000);
            } else {
                stopAndPrepareAsync();
            }
        } catch (IllegalStateException e) {
            Log.e("MediaPlayerManager", e.getMessage());
        }
    }

    /* for custom logic */
    /* to customize some OnPreparedListener's logic*/
    public interface OnPreparedListener {
        void onPrepared(MediaPlayer mp);
    }

    /* to customize some OnErrorListener's logic*/
    public interface OnErrorListener {
        boolean onError(MediaPlayer mp, int what, int extra);
    }

    /* to customize some OnPlayOrPauseListener's logic*/
    public interface OnPlayOrPauseListener {
        void onPlay();

        void onPause();
    }

    private void stopAndPrepareAsync() {
        try {
            mMediaPlayer.stop();
            mMediaPlayer.prepareAsync();
        } catch (IllegalStateException e) {
            Log.e("MediaPlayerManager", e.getMessage());
        }
    }

    private boolean ifNotEnterAUrl(String songUrl) {
        return songUrl.length() == 0;
    }

    private boolean ifNotEnterAHttpsUrl(String songUrl) {
        return (!songUrl.contains("https://"));
    }
}
