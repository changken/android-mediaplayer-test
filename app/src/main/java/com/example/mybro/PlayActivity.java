package com.example.mybro;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.TimeUnit;

public class PlayActivity extends AppCompatActivity {
    private MediaPlayer mediaPlayer;
    private Button playBtn, stopBtn;
    private SeekBar seekBar;
    private TextView nowTime, endTime;

    private double startTime = 0;
    private double finalTime = 0;
    private int doOnlyOnce = 0;

    private Handler mHandler = new Handler();

    private Runnable updateSeekbarProgress = new Runnable() {

        @Override
        public void run() {
            if (mediaPlayer != null) {
                long mCurrentPosition = mediaPlayer.getCurrentPosition();

                ViewAction.changeTvText(nowTime, String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition),
                        TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                        toMinutes(mCurrentPosition))));
                ViewAction.setSeekBarProgress(seekBar, mediaPlayer.getCurrentPosition() / 1000);
            }
            mHandler.postDelayed(this, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        init();

        ViewAction.disableButton(stopBtn);
        ViewAction.disableSeekBar(seekBar);

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAction.enableButton(stopBtn);

                if (doOnlyOnce == 0) {
                    finalTime = mediaPlayer.getDuration();
                    ViewAction.enableSeekBar(seekBar);
                    ViewAction.setSeekBarMax(seekBar, (int) finalTime / 1000);
                    doOnlyOnce = 1;
                    Log.v("PlayAcitvity", "once!");
                }

                ViewAction.changeTvText(endTime, String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                        toMinutes((long) finalTime))));

                ViewAction.changeTvText(nowTime, String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                        TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                        toMinutes((long) startTime))));

                startTime = mediaPlayer.getCurrentPosition();
                ViewAction.setSeekBarProgress(seekBar, (int) startTime / 1000);

                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    ViewAction.changeButtonText(playBtn, "撥放");
                } else {
                    mediaPlayer.start();
                    ViewAction.changeButtonText(playBtn, "暫停");
                    mHandler.postDelayed(updateSeekbarProgress, 1000);
                }
            }
        });

        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.stop();
                ViewAction.disableButton(playBtn);
                ViewAction.disableSeekBar(seekBar);

                //準備音樂
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        ViewAction.enableButton(playBtn);
                        ViewAction.changeButtonText(playBtn, "撥放");
                        ViewAction.disableButton(stopBtn);
                        doOnlyOnce = 0;
                    }
                });
                mediaPlayer.prepareAsync();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer.seekTo(progress * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void init() {
        playBtn = (Button) findViewById(R.id.play_btn);
        stopBtn = (Button) findViewById(R.id.stop_btn);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        nowTime = (TextView) findViewById(R.id.now_time);
        endTime = (TextView) findViewById(R.id.end_time);

        mediaPlayer = MediaPlayer.create(this, R.raw.billie_eilish_lovely);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mediaPlayer != null) {
            // release media source!
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
