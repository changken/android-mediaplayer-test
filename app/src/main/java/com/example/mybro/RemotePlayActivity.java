package com.example.mybro;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class RemotePlayActivity extends AppCompatActivity {
    // android ui Component
    private Button rplayBtn, rStopBtn, setSourceBtn;
    private EditText urlEditText;
    private SeekBar seekBar;
    private TextView nowTime, endTime;
    private Handler mHandler = new Handler();

    // SeekBar and MediaPlayerManager.seekTo()
    private double startTime = 0;
    private double finalTime = 0;
    // update SeekBar progress
    private Runnable updateSeekBarProgress = new Runnable() {
        @Override
        public void run() {
            if (mMediaPlayerManager.checkIfMediaPlayerExisted()) {
                setTheNowTime();
            }
            mHandler.postDelayed(this, 1000);
        }
    };

    private MediaPlayerManager mMediaPlayerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_play);

        // self signed certificate
        SSLContext ctx = null;
        try {
            ctx = SSLContext.getInstance("TLS");
            ctx.init(null, new TrustManager[]{
                    new X509TrustManager() {
                        public void checkServerTrusted(X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[]{};
                        }
                    }
            }, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(ctx.getSocketFactory());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        initComponents();

        initListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // clear media player
        mMediaPlayerManager.clearMediaPlayer();
    }

    private void initComponents() {
        rplayBtn = (Button) findViewById(R.id.rplay_btn);
        rStopBtn = (Button) findViewById(R.id.rstop_btn);
        setSourceBtn = (Button) findViewById(R.id.set_source_btn);
        urlEditText = (EditText) findViewById(R.id.url_edittext);
        seekBar = (SeekBar) findViewById(R.id.rseekBar);
        nowTime = (TextView) findViewById(R.id.rstart_time);
        endTime = (TextView) findViewById(R.id.rend_time);

        ViewAction.disableButton(rplayBtn);
        ViewAction.disableButton(rStopBtn);
        ViewAction.disableSeekBar(seekBar);

        mMediaPlayerManager = new MediaPlayerManager(new MediaPlayerManager.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                ViewAction.enableButton(setSourceBtn);
                ViewAction.enableButton(rplayBtn);
                ViewAction.changeButtonText(rplayBtn, "Play");

                // set the end time (only once time)
                ViewAction.enableSeekBar(seekBar);
                setTheEndTime();
                Log.v("RemotePlayActivity", "SeekBar ready!");
            }
        }, new MediaPlayerManager.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                ViewAction.enableButton(setSourceBtn);

                return true;
            }
        }, new MediaPlayerManager.OnPlayOrPauseListener() {
            @Override
            public void onPlay() {
                ViewAction.changeButtonText(rplayBtn, "Pause");
                mHandler.postDelayed(updateSeekBarProgress, 1000);
            }

            @Override
            public void onPause() {
                ViewAction.changeButtonText(rplayBtn, "Play");
            }
        });
    }

    private void initListeners() {
        setSourceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAction.disableButton(rplayBtn);
                mMediaPlayerManager.createMediaPlayer(urlEditText.getText().toString());
            }
        });

        rplayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAction.enableButton(rStopBtn);
                ViewAction.disableButton(setSourceBtn);

                // set the now time
                setTheNowTime();

                mMediaPlayerManager.playAndPauseTheSong();
            }
        });

        rStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewAction.disableButton(rplayBtn);
                ViewAction.disableButton(rStopBtn);
                ViewAction.disableSeekBar(seekBar);

                mMediaPlayerManager.stopAndResetTheSong();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mMediaPlayerManager.checkIfMediaPlayerExisted() && fromUser) {
                    // change the progress to which the user selected.
                    mMediaPlayerManager.seekTo(progress * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void setTheNowTime() {
        startTime = mMediaPlayerManager.getCurrentPosition();
        ViewAction.setSeekBarProgress(seekBar, (int) startTime / 1000);
        ViewAction.changeTvText(nowTime, String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                toMinutes((long) startTime))));
    }

    private void setTheEndTime() {
        finalTime = mMediaPlayerManager.getDuration();
        ViewAction.setSeekBarMax(seekBar, (int) finalTime / 1000);
        ViewAction.changeTvText(endTime, String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                toMinutes((long) finalTime))));
    }
}
