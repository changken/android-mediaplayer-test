package com.example.mybro;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;

public class TestYoutubePlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final String API_KEY = "htyhry";

    //https://www.youtube.com/watch?v=
    public static final String VIDEO_ID = "OsUr8N7t4zc";
    public static List<String> songList = new ArrayList<String>(){{
        add("87HYVE-aUSU");
        add("OsUr8N7t4zc");
        add("bUc9WjujDLc");
    }};
    private YouTubePlayerView mYoutubePlayerView;

    private Button skipBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_youtube_player);

        mYoutubePlayerView = (YouTubePlayerView) findViewById(R.id.player_view);
        mYoutubePlayerView.initialize(API_KEY, this);
        skipBtn = (Button) findViewById(R.id.skip_btn);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean wasRestored) {
        Toast.makeText(this, "onInitializationSuccess!", Toast.LENGTH_SHORT).show();
        if (youTubePlayer == null) {
            Log.d("CheckPoint", "CheckPoint youtubePlayer == null");
            return;
        }

        if (!wasRestored) {
            Log.d("CheckPoint", "CheckPoint !wasRestored");
            // youTubePlayer.cueVideo(VIDEO_ID);
            int songIndex = (int)(Math.random() * songList.size());
            youTubePlayer.loadVideo(songList.get(songIndex));
            // youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
            // youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
        }

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                youTubePlayer.pause();
            }
        });

        youTubePlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
            @Override
            public void onPlaying() {
                Log.d("CheckPoint", "CheckPoint onPlaying");
            }

            @Override
            public void onPaused() {
                Log.d("CheckPoint", "CheckPoint onPaused");

                int songIndex = (int)(Math.random() * songList.size());
                youTubePlayer.loadVideo(songList.get(songIndex));

                Log.d("CheckPoint", "CheckPoint " + youTubePlayer.getCurrentTimeMillis() + " " + youTubePlayer.getDurationMillis());
            }

            @Override
            public void onStopped() {
                Log.d("CheckPoint", "CheckPoint onStopped");
            }

            @Override
            public void onBuffering(boolean b) {
                Log.d("CheckPoint", "CheckPoint onBuffering");
            }

            @Override
            public void onSeekTo(int i) {
                Log.d("CheckPoint", "CheckPoint onSeekTo " + i);
            }
        });

        youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {
                Log.d("CheckPoint", "CheckPoint onLoading");
            }

            @Override
            public void onLoaded(String s) {
                Log.d("CheckPoint", "CheckPoint onLoaded");
            }

            @Override
            public void onAdStarted() {
                Log.d("CheckPoint", "CheckPoint onAdStarted");
            }

            @Override
            public void onVideoStarted() {
                Log.d("CheckPoint", "CheckPoint onVideoStarted");
            }

            @Override
            public void onVideoEnded() {
                Log.d("CheckPoint", "CheckPoint onVideoEnded");
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {
                Log.d("CheckPoint", "CheckPoint onError = " + errorReason);
            }
        });
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }
}
